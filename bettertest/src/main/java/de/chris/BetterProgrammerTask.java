package de.chris;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BetterProgrammerTask {

  /**
   * Aufgabe 4
   */

  private static boolean isSorted(List<Integer> l) {
    for (int i = 0; i < (l.size() - 1); i++) {
      if (l.get(i) > l.get(i + 1)) return false;
    }
    return true;
  }

  public static List<Integer> getReversalsToSort(int[] a) {
        /*
         You need to sort an array of integers by repeatedly reversing
         the order of the first several elements of it.
         For example, to sort [12,13,11,14], you need to  reverse the order of the first two (2)
         elements and get [13,12,11,14] and then reverse the order of the first three (3)
         elements and get [11,12,13,14]
         The method should return the shortest(!) possible list of integers corresponding to the required reversals.
         For the previous example, given an array [12,13,11,14]
         the method should return a list with Integers 2 and 3.
         */


    List<Integer> retval = new ArrayList<Integer>();
    if (a == null || a.length < 2) return retval;

    List<Integer> listA = new ArrayList<Integer>();
    for (int i : a) {
      listA.add(i);
    }

    int cnt = 2;
    while (!isSorted(listA)) {
      Collections.reverse(listA.subList(0, cnt));
      retval.add(cnt++);
    }

    return retval;
  }

  /**
   * Aufgabe 3
   */

  public static List<Integer> getReversalsToSort2(int[] a) {
        /*
         You need to sort an array of integers by repeatedly reversing
         the order of the first several elements of it.

         For example, to sort [12,13,11,14], you need to  reverse the order of the first two (2)
         elements and get [13,12,11,14] and then reverse the order of the first three (3)
         elements and get [11,12,13,14]

         The method should return the shortest(!) possible list of integers corresponding to the required reversals.
         For the previous example, given an array [12,13,11,14]
         the method should return a list with Integers 2 and 3.
        */


    List<Integer> arrayList = new ArrayList<Integer>();

    for (int i = 2; i < a.length; i++) {
      a = bla(a, i);
      arrayList.add(i);
    }

    return arrayList;
  }

  private static int[] bla(int[] a, int factor) {

    int[] result = new int[a.length];

    for (int i = 0; i<a.length; i++) {

      if (i<factor) {
        result[i] = a[factor-i];
      }

      result[i] = a[i];
    }

    return result;
  }

  public static boolean isListInOrder(int[] a)
  {
    if ( a.length > 1 )
    {
      int lastint = a[0];
      for ( int i = 1; i < a.length; i++)
      {
        if ( a[i] <= lastint )
          return false;
        lastint = a[i];
      }
    }
    return true;
  }

  public static int[] doSwap(int digToSwap, int[] a)
  {
    int[] swappedList = new int[a.length];
//copy list (so we dont change original)
    for ( int i = 0; i < a.length; i++)
      swappedList[i] = a[i];
//swap positions
    for ( int i = 0; i < digToSwap/2; i++)
    {
      swappedList[i] = a[digToSwap-1-i];
      swappedList[digToSwap-1-i] = a[i];
    }
    return swappedList;
  }

  static class nodeItem
  {
    List<Integer> moves = new ArrayList<Integer>();
    int[] orderedNums;
  }


  // Please do not change this interface
  public static interface Node {
    int getValue();
    List<Node> getChildren();
  }

  /**
   * Aufgabe 2
   */

  public static List<Node> traverseTreeInWidth(Node root) {
        /*
          Please implement this method to
          traverse the tree in width and return a list of all passed nodes.

          The list should start with the root node, next
          it should contain all second-level nodes, then third-level nodes etc.

          The method shall work optimally with large trees.
         */
    ArrayList<Node> nodeList = new ArrayList<Node>();

    if (root == null)
      return nodeList;

    nodeList.add(root);

    if (!root.getChildren().isEmpty()) {
      for (Node child: root.getChildren()) {
        nodeList.addAll(traverseTreeInWidth(child));
      }
    }

    return nodeList;
  }

  public static int sumOfTwoLargestElements(int[] a) {
        /*
          Please implement this method to
          return the sum of the two largest numbers in a given array.
         */

    Arrays.sort(a);

    return a[a.length-1] + a[a.length-2];
  }


  /**
   * Aufgabe 1
   */

  public static Change getCorrectChange(int cents) {
    /*
      Please implement this method to
      take cents as a parameter
      and return an equal amount in dollars and coins using the minimum number of
      coins possible.
      For example: 164 cents = 1 dollar, 2 quarters, 1 dime and 4 cents.
      Return null if the parameter is negative.

     */

    if (cents < 0 ) {
      return null;
    }


    int dollars = cents/100;
    int rest = cents-dollars*100;

    int quarters = rest/25;
    rest = rest-quarters*25;

    int dimes = rest/10;
    rest = rest-dimes*10;

    int nickels = rest/5;
    rest = rest-nickels*5;

    return new Change(dollars,quarters, dimes, nickels,rest);

  }

  public final class Waerung {
    private int cents;

    public Waerung(int cents) {

      this.cents = cents < 0 ? 0 : cents;     // Methode auslagern.
    }

    @Override
    public boolean equals(Object o) {

      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }

      Waerung waerung = (Waerung) o;

      return cents == waerung.cents;
    }

    @Override
    public int hashCode() {

      return cents;
    }
  }


  // Please do not change this class
  static class Change {
    private final int _dollars;
    private final int _quarters; //25 cents
    private final int _dimes; // 10 cents
    private final int _nickels; // 5 cents
    private final int _cents; // 1 cent


    public Change(int dollars, int quarters, int dimes, int nickels, int cents) {
      _dollars = dollars;
      _quarters = quarters;
      _dimes = dimes;
      _nickels = nickels;
      _cents = cents;
    }


    public int getDollars() {
      return _dollars;
    }


    public int getQuarters() {
      return _quarters;
    }


    public int getDimes() {
      return _dimes;
    }


    public int getNickels() {
      return _nickels;
    }


    public int getCents() {
      return _cents;
    }
  }
}

