package de.chris;

import static org.junit.Assert.assertEquals;

import de.chris.BetterProgrammerTask.Change;
import org.junit.Test;

public class BetterProgrammerTaskTest {

  @Test
  public void TestGetChange() {

    Change actual = BetterProgrammerTask.getCorrectChange(164);

    assertEquals("Dollars sollte bei 1 sein.", 1, actual.getDollars());
    assertEquals("Quarters sollte bei 2 sein.", 2, actual.getQuarters());
    assertEquals(1, actual.getDimes());
    assertEquals(0, actual.getNickels());
    assertEquals(4, actual.getCents());

  }

  @Test
  public void TestGetChangeNull() {

    Change actual = BetterProgrammerTask.getCorrectChange(0);

    assertEquals("Change sollte 0 sein.", 0, actual.getDollars());

  }

  @Test
  public void TestSumArray() {

    int[] array = { 5, 15, 10 , 7, 9};

    int actual = BetterProgrammerTask.sumOfTwoLargestElements(array);

    assertEquals("Value sollte 25 sein.", 25, actual);

  }
  @Test
  public void Test() {

    int[] array = { 5, 15, 10 , 7, 9};

    int actual = BetterProgrammerTask.sumOfTwoLargestElements(array);

    assertEquals("Value sollte 25 sein.", 25, actual);

  }

}
